# CTG Devops Academy: Ansible

For learning Ansible, I've split the knowledge up into different topics. For each topic there will be exercises building onto the solutions of the previous topic. All exercises  you' find in the `Exercices.md` file. 

## Installation

On your working machine, make sure you have installed recent versions of:

- [VirtualBox](https://virtualbox.org/)
- [Vagrant](https://vagrantup.com/)
- [Git](https://git-scm.com/) and for Windows hosts also Git Bash. If you install Git with default settings (i.e. always click "Next" in the installer), you should be fine.
- Ansible (only on Mac/Linux)

- 
## Getting started

- creating our virtual environment: `vagrant up`
- deleting our virtual environment: `vagrant destroy -f`
- SSH to our virtual environment: `vagrant ssh web`

## Contributors

- [Bert Van Vreckem](https://github.com/bertvv/): Maintainer of the vagrant skeleton on which this project is based.
