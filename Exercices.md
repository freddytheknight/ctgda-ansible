# Exercises
Each topic has relevant exercises, expanding on previous solutions.

In our lab, we will be using 3 virtual machines:
```
tower 192.168.69.10
web 192.168.69.11
db 192.168.69.12
```

use ```export TERM=xterm``` before launching any playbook to get colored output.

For easy learning purposes, do every step as a the root user on the virtual machines.

## 01. Ansible Overview

As we will configure `web` and `db` from `tower`, this should be the only machine where Ansible is installed.
So go ahead and install Ansible. 

Check the output of `ansible --version`

## 01b. Configure SSH
If want to connect to `web` and `db` over ssh without any password we should generate and distribute a ssh key pair.

1)  Generate a ssh-keypair on tower

2) Distribute the public key pair to `web` and `db`. 
notes: 
- use the   `.ssh/authorized_keys` file
- don't forget to set the right permissions to our ssh folders and files

3) Test the ssh connection from tower, no password should be given.

## 02. Inventory + preparing our environment
1) Create a folder `ansible`. This will be our working directory from now on.
2) Create a inventory file for our 2 virtual machines. Split into 2 groups:  `web` and `db`. 
3) Create a `ansible.cfg` file in the same directory as our inventory file. Configure Ansible to:
- write logs to `ansible.log`
- run our ansible-playbook without using the `-i` parameter
- disable retry-files
- disable host key checking
- run playbooks at 2 servers simultaneously time instead of going one by one.

## 03. Playbooks
1) Create our first playbook that prints out all facts of the `web` vm.
2) On all hosts
- install the vim package

3) On all web servers
- execute the command `cat /etc/hosts`. Output should be shown to the user. On how to do this, read: https://stackoverflow.com/questions/18789545/displaying-output-of-a-remote-command-with-ansible

-  install the httpd package
-  start the httpd service

4) On all db servers
-  install the `mariadb-server` package
-  start the mariadb service  
 
Hints:
- name all tasks accordingly
- although not best practice, only use the Ansible `command` module for now: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html
- If the installation of packages is "stuck":  Add a custom parameter to `yum` to skip confirmation

## 04. Modules
1) Transform the `site.yml` from the previous exercise, using modules this time. You can delete everything relating to the `cat` exercise.
2) Edit the `/etc/hosts` file on each db server so you can ping the webserver by hostname `web`. 
3) On each server, create a user called `ctg`. This user should have no home directory.
4) On `db`, create a file  `/tmp/foobar`, user and group should be `ctg` 

## 05. Variables
-   create 2 variables: name, age and flower. Use these variables to echo the line "Hello! I'm $name, $age years old and my favorite flower is a $flower" on `web`. 
-  create  a variable `file_location` and use in for placing `/tmp/foobar`

## 06. Loops
1) add a variable `fruits` on db containing: `apple`, `banana`, `Grape` and `orange`. Apply a loop directive to a task to print all fruits defined in the `fruits` variable.
2) Add the name of each participant of the CTG academy on a new line in  `/tmp/foobar`
3) For a more realistic use case. Instead of only installing vim, install vim, curl, tree, telnet and zip.

## 07. Conditionals
1) Write two tasks on `web` attempting to print if I am a child or an Adult. Use the when conditional to print if I am a child or an Adult based on weather my age is `< 18 (child)` or `>= 18 (Adult)`.

## 08. Roles

### Web
Our goal is to write a role that will install and configure apache (httpd) on our `web` vm. Start by deleting all tasks we wrote before before commencing on this exercise.

1) Create a `roles` and `roles/apache` directory.
2) Create all official role subdirectories and their content inside the `apache` directory
3) Create your first task file
 -   Make sure httpd is installed
-   Make sure httpd is started and enabled
-   create a vhost directory in `/var/www/vhosts/{{ ansible_hostname }}`
4) Create a basic [index.html](https://www.w3schools.com/html/html_basic.asp) page and place it in our vhost directory
5) Install the apache configuration:
- Install the template provided in ``/etc/httpd/conf.d/vhost.conf``. Mode should be `0644`.
- Use a handler to restart httpd after an configuration update.
- Use a ansible fact at `ServerName`  to use the ip in the `192.168.69.0` range.
 hint: use `ansible web -m setup`

```
<VirtualHost *:80>
    ServerName {{ CHANGE_ME }}
    ErrorLog logs/{{ ansible_hostname }}-error.log
    CustomLog logs/{{ ansible_hostname }}-common.log common
    DocumentRoot /var/www/vhosts/{{ ansible_hostname }}/

    <Directory /var/www/vhosts/{{ ansible_hostname }}/>
      Options +Indexes +FollowSymlinks +Includes
      Order allow,deny
      Allow from all
    </Directory>
</VirtualHost>
```
6) From tower, run a curl to check everything is working like it should be.
```
[root@tower ansible]# curl 192.168.69.11
 <!DOCTYPE html>
<html>
<body>

<h1>My First Heading</h1>
<p>My first paragraph.</p>

</body>
</html>
```
## Bonus exercises: community playbooks

Instead of writing all playbooks by yourself, its worth surfing the internet finding community playbooks.

Before you start on the exercises below, have a read at [the official docs](https://galaxy.ansible.com/docs/) on how to get started.

## DB
note: before starting this exercise. delete the old `db` vm with vagrant and start a new one. Don't forget to enable password less ssh again.

1) Use the https://github.com/geerlingguy/ansible-role-mysql playbook to install and configure a MySQL database.
```
db name: ctg
db user: ctg
password: 1234boomboom1234
priviliges: ctg user has all permissions on the db
db connections can be made from everywhere (=%)
```

2) use https://github.com/geerlingguy/ansible-role-firewall to open the MySQL port.

