# Inventory

You will be usually running the ansible tasks against a large number of systems. You need to store the details of these systems in some files. In ansible, this file is called Inventory file. By default the inventory file is _**/etc/ansible/hosts**_ . If you want to use another inventory file you can specify it during execution by using _**-i <path of inventory file>**_.

`
eg: ansible-playbook -i inventory.ini install_LAMP
`

You can also change the default inventory file in ansible configuration file. Add / modify the following variable in /etc/ansible/ansible.cfg to the new path.

`
inventory = /etc/ansible/newhosts
`

### Basic Inventory file
```
examplehost ssh_user=fedora
examplehost2 ssh_user=fedora
10.10.10.10
```

Here `examplehost` is the host name of the first system. As you already know ansible will be connecting to the remote system via SSH by default. The `ssh_user` parameter gives the ssh login name.

You can also give IP address instead of hostname. In case you do not specify the ssh_user then ansible will try SSH with the same user which you are running the script.

### Inventory file with Groups

You can also group together a set of systems in inventory file. This is helpful when you need to run a particular set of tasks against some systems and another set of tasks against another set of systems.

Inventory file for a LAMP installation
```
[Apache_servers]
apache_system1

[MySQL_servers]
MySQL_system1
MySQL_system2

[PHP_servers]
PHP_system1
PHP_system2
apache_system1
```

Here there are three groups which are denoted by the _**square brackets,** Apache_servers,_ MySQL_servers and PHP_servers. We can specify a particular role should run against MySQL_servers. So this will run on all the systems under that group. We can also specify the same system in multiple groups like apache_system1 shown in the example.

If you are running the playbook Ad-hoc command then you can use the groups like following.

`
ansible MySQL_servers -m shell -a 'echo MySQL systems'
`

### Group of Groups

You can also make a group a subset of another group. Suppose you want to add a file in all the systems in the LAMP installation, you need not have a separate group. Instead, you can do the following.

```
[LAMP:children]
Apache_servers
MySQL_servers
PHP_servers
```

### **Additional Parameters in Inventory File**

You have already seen some parameter like ssh_user. There are some more parameters like this for further specifications.

```
[MySQL_servers]
MySQL_system1 ansible_user=fedora ansible_port=22 ansible_ssh_pass="dontdothis"
MySQL_system2 ansible_connection=local
```

`ansible_user` – The ssh user to use. For MySQL we will be login as fedora user


`ansible_port` – The port to use. For SSH we will be using 22 by default. If you want to use some other port then you can mention it here.



`ansible_ssh_pass` – If you haven’t set the passwordless entry then you might need to specify an ssh password also. But don’t give the password in plain text. You can encrypt it via Ansible vault.

## Sources
https://www.mydailytutorials.com/ansible-inventory-file-tutorial/

