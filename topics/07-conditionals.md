#  Conditionals

In Ansible, you can define conditions that will be evaluated before a task is executed. When a condition is not met, the task is then skipped. This is done with the `when` keyword, which accepts expressions that are typically based on a variable or a fact.

The following example defines two variables: `create_user_file` and `user`. When the `create_user_file` is evaluated to `true`, a new file will be created in the home directory of the user defined by the `user` variable:

```yml
---
- hosts: all
  vars:
    - create_user_file: yes
    - user: sammy
  tasks:
    - name: create file for user
      file:
        path: /home/{{ user }}/myfile
        state: touch
      when: create_user_file
```
A common use for conditionals in the context of Ansible playbooks is to combine them with `register`, a keyword that creates a new variable and assigns it with the output obtained from a command. This way, you can use any external command to evaluate the execution of a task.

One important thing to notice is that, by default, Ansible will interrupt a play if the command you’re using to evaluate a condition fails. For that reason, you’ll need to include an `ignore_errors` directive set to `yes` in said task, and this will make Ansible move on to the next task and continue the play.

The following example will only create a new file in the user home directory in case that file doesn’t exist yet, which we’ll test with an `ls` command. If the file exists, however, we’ll show a message using the `debug` module.

```yml
---
- hosts: all
  vars:
    - user: sammy
  tasks:
    - name: Check if file already exists
      command: ls /home/{{ user }}/myfile
      register: file_exists
      ignore_errors: yes

    - name: create file for user
      file:
        path: /home/{{ user }}/myfile
        state: touch
      when: file_exists is failed

    - name: show message if file exists
      debug:
        msg: The user file already exists.
      when: file_exists is succeeded
```
The first time you run this playbook, the command will fail because the file doesn’t exist in that path. The task that creates the file will then be executed, while the last task will be skipped.

## Sources 
- https://www.digitalocean.com/community/tutorials/how-to-use-conditionals-in-ansible-playbooks
