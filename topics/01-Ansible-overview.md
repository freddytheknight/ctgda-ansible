# Introduction to Ansible

## WHAT IS ANSIBLE: overview video
[click here](https://www.youtube.com/watch?v=wgQ3rHFTM4E)
Alot what is discussed in this video will be further explored in the topics of this course.

## WHAT IS ANSIBLE
Ansible is an open source IT Configuration Management, Deployment & Orchestration tool. It aims to provide large productivity gains to a wide variety of automation challenges.

> **_Ansible_** _is the first human-readable automation language that can be read and written across IT._

No one likes repetitive tasks. With **Ansible**, IT admins can begin automating away the drudgery from their daily tasks. Ansible is a **simple automation language** that can perfectly describe an IT application infrastructure.

## WHY ANSIBLE ?

-   **Free Tool :** Ansible is an open-source tool.
-   **Agentless :** Ansible is completely agentless. There are no agents/software or additional firewall ports that you need to install on the client systems or hosts which you want to automate.
-   **Easy to Learn :** It’s easy-to-learn, self-documenting, and doesn’t require a grad-level computer science degree to read. Automation shouldn’t be more complex than the tasks it’s replacing.
-   **Simple to Understand :** Ansible is written in Python and uses YAML for playbooks. Both Python and YAML are human-readable languages.
-   **Idempotent :** Ansible keeps track of the state of resources in managed systems in order to avoid repeating tasks that were executed before. If a package was already installed, it won’t try to install it again. No matter how many times you call the operation, the result will be the same.

![](https://miro.medium.com/max/1276/1*bkhAqB6bsx0WIyYi__1ITg.jpeg)
## BASH SCRIPT vs ANSIBLE
A bash script does an action. That could be copying a file, or installing a package, or modifying permissions, etc. What happens if you re-run the script? You might get an error if the system is already in the desired state. I suppose you could put a lot of error checking in to make sure you don't have unintended consequences on running it again. And you could have it report on if it had to make the change vs. if the change has already been done and the system is in the state you wanted it to be. Except at that point, you've now written your own version of Ansible.

The way I often explain the difference is this:

-   Bash (or another scripting language) describes actions. Do this thing. Now do this other thing.
    
-   Ansible (and other configuration management systems) describe the desired state. This file should exist here, owned by this user, with these permissions. This package should be installed. This line in this config file should appear like this.
    

By describing the desired state of things it lets you get away from worrying about how to make that happen and focus on what needs to happen. For example, if I need file A to be in location B, I can just say "file A should be at B". I don't have to write the code to check if the file already exists at B, and if not then copy from A to B, but if it did exist to check if the file at B matches A and if not do the copy anyway. I can just say "file A should be at B".

Another benefit of Ansible is you get a listing of what is already in the desired state and what changed. You can re-run it periodically to prevent configuration drift, and see what changed. Did some coworker edit a config file to test something and forgot to change it back? Configuration management would catch that and put it back the way it should be. Did you re-run it and got 100% OKs and no changes? Enjoy that warm fuzzy because now you KNOW your environment is how you want it to be.

Having said all that, if you only need something to perform an action once, or infrequently, but it's too complex/annoying/tedious to type out on the command line, perhaps a bash script might be the best solution.

## ARCHITECTURE
![](https://miro.medium.com/max/1394/1*MXuctk_u5cRU_yGRYQkORA.png)
**Ansible Controller or Management Node**

-   The machine where Ansible is installed, responsible for running the provisioning on the servers you are managing (known as Hosts).

**Ansible basics: Ad-hoc commands, Playbooks and Inventories**

-   **Ad-hoc commands**: are command line “inline” commands that can run and return a result. These commands are associated with the ansible executable.
-   **Playbooks**: are files that contain many plays and tasks that include multiple operations in YAML format. These are associated with the ansible-playbook executable.
-   **Inventories:** Regardless of the way you use Ansible (ad-hoc, playbook), you will need a list of hosts that you want to manage. Inventory is an initialization file that contains information about the hosts you are managing.

## Sources
- https://www.reddit.com/r/linuxadmin/comments/emcuqm/ansible_versus_bash_script/
- https://chetna-manku.medium.com/ansible-and-how-nasa-is-using-ansible-ae78838f71ce
- https://www.udemy.com/course/learn-ansible

