# Playbooks
## Introduction

**Ansible Playbooks** are the files where Ansible code is written. These files are written in the language, **YAML**, which is a funny acronym for, “YAML Ain’t no Markup Language.”

Playbooks contain one or more Plays. **Plays** map a group of computers to a few well-defined roles known as Tasks. **Tasks** can be defined as Ansible scripts.

## Yaml
YAML is a simple language that consists of key-value pairs. While not mandatory, `---` marks the beginning of a YAML program, and `...` marks the end.
```yaml
john: #variable declared here
#data defined in following lines:
name: john doe
address: new york city
gender: male
...
```
The previous example defines the name, address, and gender of a fictionary person (John Doe) in the `john` variable.

YAML also allows the creation of lists. Lists can be created with the `-` symbol inside variables to define elements.

The following example defines a list of cities with their full name and state.

```yaml
---
cities:
  - newYorkCity: 
    name: new york city
    state: new york
  - losAngeles:
    name: los angeles
     state: california
  - philadelphia: 
    name: philadelphia
    state: pennsylvania
...
```
## Usage of Ansible playbook

For every play, a number of hosts and remote users are defined.

The `hosts` are a list of machines. 

The tasks for a play are defined in the `tasks` key.
```yaml
---
- hosts: all
  tasks:
    - name: copy file
      win_copy:
        src: C:\source.txt
        dest: C:\destination\
        remote_src: yes
...
```
This example copies a file from a local Windows server to a remote Windows host. The actual functionality is defined in the `win-copy` line. The directory location for the source file is entered in `src`, and the directory location for the destination is entered in `dest.`

## The Different YAML Tags

Let us now go through the different YAML tags. The different tags are described below −

### name

This tag specifies the name of the Ansible playbook. As in what this playbook will be doing. Any logical name can be given to the playbook.

### hosts

This tag specifies the lists of hosts or host group against which we want to run the task. The hosts field/tag is mandatory. It tells Ansible on which hosts to run the listed tasks. The tasks can be run on the same machine or on a remote machine. One can run the tasks on multiple machines and hence hosts tag can have a group of hosts’ entry as well.

### vars

Vars tag lets you define the variables which you can use in your playbook. Usage is similar to variables in any programming language.

### tasks

All playbooks should contain tasks or a list of tasks to be executed. Tasks are a list of actions one needs to perform. A tasks field contains the name of the task. This works as the help text for the user. It is not mandatory but proves useful in debugging the playbook. Each task internally links to a piece of code called a module. A module that should be executed, and arguments that are required for the module you want to execute.

![https://www.middlewareinventory.com/wp-content/uploads/2019/07/Screenshot-2020-07-15-at-1.47.21-PM.png](https://www.middlewareinventory.com/wp-content/uploads/2019/07/Screenshot-2020-07-15-at-1.47.21-PM.png)

## How to run a playbook

Ansible playbook can be executed with `ansible-playbook` command. 

```bash
ansible-playbook $myplaybook.yml -i $myinventory
```
## Sources
- https://www.educative.io/edpresso/what-is-ansible-playbook
- https://www.middlewareinventory.com/wp-content/uploads/2019/07/Screenshot-2020-07-15-at-1.47.21-PM.png
- https://www.tutorialspoint.com/ansible/ansible_playbooks.htm
