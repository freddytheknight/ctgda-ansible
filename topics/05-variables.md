#  Variables
## What Constitutes a Valid Variable Name?

A variable name includes letters, numbers, underscores or a mix of either 2 or all of them. However, bear in mind that a variable name must always begin with a letter and should not contain spaces.

Let’s take a look a few examples of valid and unacceptable variable names:
```
football 
foot_ball
football20 
foot_ball20
```
Non-valid Variable Name Examples:
```
foot ball
20 
foot-ball
```
## Types of variables
### 1. Playbook Variables

**Playbook** variables are quite easy and straightforward. To define a variable in a **playbook**, simply use the keyword **vars** before writing your variables with indentation.

To access the value of the variable, place it between the double curly braces enclosed with quotation marks.

Here’s a simple playbook example:
```yaml
- hosts: all
  vars:
    **greeting**: Hello world! 

  tasks:
  - name: Ansible Basic Variable Example
    debug:
      msg: "{{ **greeting** }}"
```

In the above playbook, the **greeting** variable is substituted by the value **Hello world!** when the playbook is run. The playbook simply prints the message **Hello world!** when executed.

Additionally, you can have a list or an array of variables as shown:

The playbook below shows a variable called **continents**. The variable holds 5 different values – continent names. Each of these values can easily be accessed using **index 0** as the first variable.

The example of the playbook below retrieves and displays **Asia (Index 1)**.

```yaml
- hosts: all
  vars:
    continents:
      - Africa
      - Asia
      - South America
      - North America
      - Europe
      
  tasks:
  - name: Ansible List variable Example
    debug:
      msg: "{{ continents [1] }}"
```
The variable list can similarly be structured as shown:
```yaml
vars:
    Continents: [Africa, Asia, South America, North America, Europe]
 ```
 
To list all the items on the list, use the **with_items** module. This will loop through all the values in the array.
```yaml
- hosts: all
  vars:
    continents: [Africa, Asia, South America, North America, Europe]

  tasks:
  - name: Ansible array variables example
    debug: 
      msg: "{{ **item** }}"
    with_items:
      - "{{ **continents** }}"
 ```
 
## Dictionary variables
**Dictionary** variables are additionally supported in the playbook. To define the dictionary variable, simply ident the key-value pair just below the dictionary variable name.

```yaml
hosts: switch_f01

vars:
   http_port: 8080
   default_gateway: 10.200.50.1
   vlans:
       id: 10
       port: 2
```

In the example above, **vlans** is the dictionary variable while **id** and **port** are the key-value pairs.
```yaml
hosts: switch_f01

vars:
   http_port: 8080
   default_gateway: 192.168.69.1
   vlans:
      id: 10
      port: 20

 tasks:
   name: Configure default gateway
   system_configs:
     default_gateway_ip: “{{ default_gateway  }}“

   name: Label port on vlan 10
   vlan_config:
     vlan_id: "{{ vlans[‘id’] }}“
     port_id: 1/1/{{ vlans[‘port’] }}
```

For **port_id**, since we are starting the value with text and not the variable, quotation marks are not necessary to surround the curly braces.

## Build-in variables
Ansible provides a list of predefined variables that can be referenced in [Jinja2 templates and playbooks](https://www.tecmint.com/create-ansible-templates-to-create-configuration-for-nodes/) but cannot be altered or defined by the user.

Collectively, the list of Ansible predefined variables is referred to as **Ansible facts** and these are gathered when a playbook is executed.

To get a list of all the Ansible variables, use the **setup** module in the [Ansible ad-hoc command](https://www.tecmint.com/configure-ansible-managed-nodes-and-run-ad-hoc-commands/) as shown below:
```bash
$ ansible -m setup hostname
```
From the output, we can see that some of the examples of Ansible special variables include:
```
ansible_architecture
ansible_bios_date
ansible_bios_version
ansible_date_time
ansible_machine
ansible_memefree_mb
ansible_os_family
ansible_selinux
```

When running playbooks, the first task that Ansible does is the execution of setup task. I’m pretty sure that you must have come across the output:
```
TASK:  [Gathering facts] *********
```
Ansible facts are nothing but system properties or pieces of information about remote nodes that you have connected to. This information includes the System architecture, the OS version, BIOS information, system time and date, system uptime, IP address, and hardware information to mention just a few.

## Inventory Variables
Lastly, on the list, we have Ansible inventory variables. An inventory is a file in **INI** format that contains all the hosts to be managed by Ansible.

In inventories, you can assign a variable to a host system and later use it in a playbook.
```
[web]
web_server_1 ansible_user=centos http_port=80
web_server_2 ansible_user=ubuntu http_port=8080
```
The above can be represented in a playbook **YAML** file as shown:
```yaml
---
web_servers:
  web_server_1:
    ansible_user=centos
    http_port=80

  web_server_2:
    ansible_user=ubuntu
    http_port=8080
```

If the host systems share the same variables, you can define another group in the inventory file to make it less cumbersome and avoid unnecessary repetition.

For example:
```
[web_servers]
web_server_1 ansible_user=centos http_port=80
web_server_2 ansible_user=centos http_port=80
```
The above can be structured as:
```
[web_servers]
web_server_1
web_server_2

[web_servers:vars]
ansible_user=centos
http_port=80
```

And in the playbook **YAML** file, this will be defined as shown:
```yaml
---
web_servers: 
  hosts: 
    web_server_1:
    web_server_2:
  vars: 
    ansible_user=centos
    http_port=80
```
## Sources
- https://www.tecmint.com/ansible-variables-and-facts/
