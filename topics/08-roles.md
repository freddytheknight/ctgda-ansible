# Roles

## introduction
As the number and variety of systems that you manage with a single Ansible control node become more complex, it makes sense to group tasks together into [Ansible playbooks](https://www.digitalocean.com/community/tutorials/how-to-create-ansible-playbooks-to-automate-system-configuration-on-ubuntu). Using playbooks eliminates the need to run many individual tasks on remote systems, instead letting you configure entire environments at once with a single file.

However, playbooks can become complex when they are responsible for configuring many different systems with multiple tasks for each system, so Ansible also lets you organize tasks in a directory structure called a **Role**. In this configuration, playbooks invoke roles instead of tasks, so you can still group tasks together and then reuse roles in other playbooks. Roles also allow you to collect templates, static files, and variables along with your tasks in one structured format.

## What is an Ansible Role?

In the previous topics, you learned how to run the core learned how to collect tasks into playbooks and run them using the `ansible-playbook` command. The next step in the progression from running single commands, to tasks, to playbooks is to reorganize everything using an Ansible role.

Roles are a level of abstraction on top of tasks and playbooks that let you structure your Ansible configuration in a modular and reusable format. As you add more and more functionality and flexibility to your playbooks, they can become unwieldy and difficult to maintain. Roles allow you to break down a complex playbook into separate, smaller chunks that can be coordinated by a central entry point. 

For example, your top-level playbook can looks as simple as:

```yaml
---
- hosts: web
  roles:
    - apache
  vars:
    doc_root: /var/www/example
```

The entire set of tasks to be carried out to configure an Apache web server will be contained in the `apache` role that we will create. The roll will define all the tasks that need to be completed to install Apache, instead of listing each task individually like we did in previous topics.

Organizing your Ansible setup into roles allows you to reuse common configuration steps between different types of servers. Even though this is also possible by including multiple task files in a single playbook, roles rely on a known directory structure and file name conventions to automatically load files that will be used within the play.

In general, the idea behind roles is to allow you to share and reuse tasks using a consistent structure, while making it easy to maintain them without duplicating tasks for all your infrastructure.

## Creating a Role

To create an Ansible role you will need a specifically laid out directory structure. Roles always need this directory layout so that Ansible can find and use them.

The directory structure is as follows:
```bash
$ tree myrole/ 
myrole/  
├── defaults 
│  └── main.yml 
├── files 
├── handlers 
│  └── main.yml 
├── meta 
│  └── main.yml 
├── README.md 
├── tasks 
│  └── main.yml 
├── templates 
└── vars 
   └── main.yml
```
These directories will contain all of the code to implement our role. Many roles will only use one or a few of these directories depending on the complexity of the tasks involved. When you are writing your own roles, you may not need to create all of these directories.

Here is a description of what each directory represents:

-   **`defaults`**: This directory lets you set default variables for included or dependent roles. Any defaults set here can be overridden in playbooks or inventory files.
-   **`files`**: This directory contains static files and script files that might be copied to or executed on a remote server.
-   **`handlers`**: All handlers that were in your playbook previously can now be added into this directory.
-   **`meta`**: This directory is reserved for role metadata, typically used for dependency management.. For example, you can define a list of roles that must be applied before the current role is invoked.
-   **`templates`**: This directory is reserved for templates that will generate files on remote hosts. Templates typically use variables defined on files located in the `vars` directory, and on host information that is collected at runtime.
-   **`tasks`**: This directory contains one or more files with tasks that would normally be defined in the `tasks` section of a regular Ansible playbook. These tasks can directly reference files and templates contained in their respective directories within the role, without the need to provide a full path to the file.
-   **`vars`**: Variables for a role can be specified in files inside this directory and then referenced elsewhere in a role.

If a file called `main.yml` exists in a directory, its contents will be automatically added to the playbook that calls the role. However, this does not apply to `files` and `templates` directories, since their contents need to be referenced explicitly.

## Tasks 
Generally speaking, your role will execute one or more tasks to configure the target system according to the role's requirements. In this case, you'll want to install and configure Vim. By default, when you execute a role, it looks for a file named `main.yml` in the `tasks` subdirectory and execute all the tasks listed within it. You can break the tasks into multiple files for more complex roles and call them from `main.yml` using the `include_tasks` or `import_tasks` modules.

For this role, include all required tasks in the `tasks/main.yml` file:

```shell
$ vim tasks/main.yml

---
# tasks file for vim
- name: Install required packages
  package:
    name: "{{ install_packages }}"
    state: present
  become: yes

- name: Ensure .vim/{autoload,bundle} directory exists
  file:
    path: "{{ item }}"
    state: directory
    recurse: no
    mode: 0750
  loop:
    - "{{ vim_dir }}"
    - "{{ vim_dir }}/autoload"
    - "{{ vim_dir }}/bundle"

- name: Ensure Pathogen is in place
  get_url:
    dest: "{{ vim_dir }}/autoload/pathogen.vim"
    url: https://tpo.pe/pathogen.vim

- name: Deploy plugins
  git:
    dest: "{{ vim_dir }}/bundle/{{ item.name }}"
    repo: "{{ item.url }}"
    clone: yes
    update: yes
    recursive: no
  loop: "{{ plugins }}"

```

Notice that you don't include the list of packages or plugins to install directly with the task definition. Instead, you're using the variables `install_packages` and `plugins`.

By defining variables instead of hard coding the values, you make your roles more reusable and easier to maintain. 

## Default variables
When you're developing an Ansible role, you might want to allow role users to provide values to customize how the role performs its tasks. These variables make your role more reusable, allowing users to modify the outcome based on their specific requirements.

For this example, the `plugins` variable allows the users to specify which plugins they want to install with Vim, making the role flexible for their needs. It's recommended to define a default value for it in the `defaults/main.yml` file to ensure that the roles execute successfully even if the user does not provide a value to this variable.

This file defines variables with a very low precedence which means Ansible will only use them in case the value wasn't defined anywhere else.

Now, define the default value for the `plugins` variable like this:

```shell
$ vim defaults/main.yml

---
install_packages:
  - vim-enhanced
  - git
  - powerline-fonts
  - fzf
vim_dir: "{{ ansible_env.HOME }}/.vim"
vimrc: "{{ ansible_env.HOME }}/.vimrc"

plugins:
  - name: vim-airline
    url: https://github.com/vim-airline/vim-airline
  - name: nerdtree
    url: https://github.com/preservim/nerdtree
  - name: fzf-vim
    url: https://github.com/junegunn/fzf.vim
  - name: vim-gitgutter
    url: https://github.com/airblade/vim-gitgutter
  - name: vim-fugitive
    url: https://github.com/tpope/vim-fugitive
  - name: vim-floaterm
    url: https://github.com/voldikss/vim-floaterm
```

 If you call the role without providing a value for this variable, it will install 4 packages and six plugins by default.

## Files folder

By default, when using the `copy` module as a role task, it will look for files to copy in the `files` subdirectory. Define the `vimrc` file like this:

```shell
$ vim files/vimrc

execute pathogen#infect()
syntax on
filetype plugin indent on

colo darkblue

" Configuration vim Airline
set laststatus=2

let g:airline#extensions#tabline#enabled=1
let g:airline_powerline_fonts=1

" Configuration NERDTree
map <F5> :NERDTreeToggle<CR>

" Configuration floaterm
let g:floaterm_keymap_toggle = '<F12>'
let g:floaterm_width = 0.9
let g:floaterm_height = 0.9

" Configuration Vim.FZF
let g:fzf_preview_window = 'right:50%'
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6  }  }
```

and in our task:
```yaml 
- name: Ensure .vimrc config in place
  copy:
    src: vimrc
    dest: "{{ vimrc }}"
    backup: yes
    mode: 0640
```

## Templates
Templates are simple text files that we use in Ansible which contains all your configuration parameters. During the playbook execution, depending on the conditions like which cluster you are using, the variables will be replaced with the relevant values. Most of the time we use them to replace configuration files or place some documents on the server. We can do much more than replacing the variables with the help of Jinj2 templating engine. We can have conditional statements, loops, filters for transforming the data, do arithmetic calculations, etc. The template files will usually have the .j2 extension, which denotes the Jinja2 templating engine used.

### A Basic Example
In the following task, I am using the template module on the example1.j2 file which will replace the default variables with values given in the playbook.

```shell
File: Playbook.yml

---
- hosts: all
  vars:
    variable1: 'Hello...!!!'
    variable2: 'My first playbook using template'
  tasks:
    - name: Basic Template Example
      template:
        src: example1.j2
        dest: /home/knoldus/Documents/Ansible/output.txt
```
```shell
File: example1.j2

{{ variable1 }}
No effects on this line
{{ variable2 }}
```
```shell
File: output.txt

Hello...!!!
No effects on this line
My first playbook using template
```

As you can see, both variables in the example1.j2 are replaced by their values. At the bare minimum, you need to have two parameters when using the Ansible template module.

-   **src:** The source of the template file. This can be a relative or absolute path. Ansible will look in the templates folder by default
-   **dest:** The destination path on the remote server

### Using lists in Ansible templates

In the next example, I’ll be using the template module to print all the items present in a list using the for the loop.

```shell
File: Playbook.yml

- hosts: all
  vars:
    list1: ['Apple','Banana','Cat', 'Dog']
  tasks:
    - name: Template Loop example.
    - template:
        src: example2.j2
        dest: /home/knoldus/Documents/Ansible/output.txt
```
```shell
File: example2.j2

Example of template module loop with a list.
{% for item in list1 %}
  {{ item }}
{% endfor %}
```
```shell
File: output.txt

Example of template module loop with a list.
Apple
Banana
Cat
Dog
```

We can see that after each iteration, a newline is added. The more practical approach of for loops is when we have to add a specific port to a large number of hosts and it is too tedious to manually update the inventory file. We can simply write the following code to update the same

```
host.list={% for node in groups['nodes'] %}{{ node }}:5672{% endfor %}
```

## Sources
- https://www.digitalocean.com/community/tutorials/how-to-use-ansible-roles-to-abstract-your-infrastructure-environment
- https://www.tutorialspoint.com/ansible/ansible_roles.htm
- https://www.redhat.com/sysadmin/developing-ansible-role
- https://blog.knoldus.com/ansible-playbook-using-templates/
