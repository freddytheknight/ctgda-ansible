# Modules
## Introduction
Ansible modules are discrete units of code which can be used from the command line or in a playbook task.

The modules also referred to as task plugins or library plugins in the Ansible.

Ansible ships with several modules that are called **module library**, which can be executed directly or remote hosts through the playbook.

Users can also write their modules. These modules can control like **services, system resources, files,** or **packages,** etc. and handle executing system commands.

Let's see how to execute three different modules from the command line.
```bash
$  ansible webservers -m service -a "name=httpd state=started"
$  ansible webservers -m ping
$  ansible webservers -m command -a "/sbin/reboot -t now"
```

Each module supports taking arguments. Mainly all modules take **key=value** arguments, space delimited.

Some module takes no arguments, and the shell/command modules take the string of the command which you want to execute.

From playbook, Ansible modules execute in a very similar way, such as:

```yaml
- name: reboot the servers
  command: /sbin/reboot -t now
```

Here is another way to pass arguments to a module that is using **YAML syntax**, and it is also called complex args.

```yaml
 - name: restart webserver
   service:
     name: httpd
     state: restarted
```

Technically, all modules return **JSON** format data, though command line or playbooks, you don't need to know much about that. If you're writing your module, it means you do not have to write modules in any particular language which you get to choose.

Modules should be idempotent and avoid making any changes if they detect that the current state matches the desired final state. When using Ansible playbooks, these modules can trigger "**change events**" in the form of notifying "**handlers**" to run additional tasks.

Documentation for each module can be accessed from the command line with the Ansible-doc tool:
```bash
 $ ansible-doc yum
```
## Commonly used Ansible modules
### Module 1: Package management

There is a module for most popular package managers, such as DNF and APT, to enable you to install any package on a system. Functionality depends entirely on the package manager, but usually these modules can install, upgrade, downgrade, remove, and list packages. The names of relevant modules are easy to guess. For example, the DNF module is [dnf_module](https://docs.ansible.com/ansible/latest/modules/dnf_module.html), the old YUM module (required for Python 2 compatibility) is [yum_module](https://docs.ansible.com/ansible/latest/modules/yum_module.html), while the APT module is [apt_module](https://docs.ansible.com/ansible/latest/modules/apt_module.html), the Slackpkg module is [slackpkg_module](https://docs.ansible.com/ansible/latest/modules/slackpkg_module.html), and so on.

Example 1:
```yaml
- name: install the latest version of Apache and MariaDB 
  dnf: 
    name:  
      - httpd  
      - mariadb-server 
    state: latest
```

### Example 2:
```yaml
- name: Install a list of packages 
  yum: 
    name:  
      - nginx  
      - postgresql  
      - postgresql-server 
    state: present
```
 
This installs the list of packages and helps download multiple packages.

## Module 2: Service

After installing a package, you need a module to start it. The [service module](https://docs.ansible.com/ansible/latest/modules/service_module.html) enables you to start, stop, and reload installed packages; this comes in pretty handy.

### Example 1:

```yaml
- name: Start service foo, based on running process /usr/bin/foo 
  service: 
    name: foo 
    pattern: /usr/bin/foo 
    state: started
```
This starts the service **foo**.

## Module 3: Copy

The [copy module](https://docs.ansible.com/ansible/latest/modules/copy_module.html) copies a file from the local or remote machine to a location on the remote machine.

### Example 1:
```yaml
- name: Copy a new "ntp.conf file into place, backing up the original if it differs from the copied version  
  copy:  
    src: /mine/ntp.conf  
    dest: /etc/ntp.conf  
    owner: root  
    group: root  
    mode: '0644'  
    backup: yes
```
### Example 2:
```yaml
- name: Copy file with owner and permission, using symbolic representation 
  copy: 
    src: /srv/myfiles/foo.conf 
    dest: /etc/foo.conf 
    owner: foo 
    group: foo 
    mode: u=rw,g=r,o=r
```
## Module 4: Debug

The [debug module](https://docs.ansible.com/ansible/latest/modules/debug_module.html) prints statements during execution and can be useful for debugging variables or expressions without having to halt the playbook.

### Example 1:
```yaml
- name: Display all variables/facts known for a host 
  debug: 
    var: hostvars[inventory_hostname]    
    verbosity: 4
```
This displays all the variable information for a host that is defined in the inventory file.

### Example 2:

```yaml
- name: Write some content in a file /tmp/foo.txt 
  copy: 
    dest: /tmp/foo.txt 
    content: |  
Good Morning!  Awesome sunshine today. 
    register: display_file_content 
- name: Debug display_file_content 
  debug: 
    var: display_file_content 
    verbosity: 2
```
This registers the content of the copy module output and displays it only when you specify verbosity as 2. 

## Module 5: File

The [file module](https://docs.ansible.com/ansible/latest/modules/file_module.html) manages the file and its properties.

-   It sets attributes of files, symlinks, or directories.
-   It also removes files, symlinks, or directories.

### Example 1:

```yaml
- name: Change file ownership, group and permissions 
  file: 
    path: /etc/foo.conf 
    owner: foo 
    group: foo 
    mode: '0644'
```
This creates a file named **foo.conf** and sets the permission to **0644**.

### Example 2:

```yaml
- name: Create a directory if it does not exist 
  file: 
    path: /etc/some_directory 
    state: directory 
    mode: '0755'
```
This creates a directory named **some_directory** and sets the permission to **0755**.

## Module 6: Lineinfile

The [lineinfile module](https://docs.ansible.com/ansible/latest/modules/lineinfile_module.html) manages lines in a text file.

-   It ensures a particular line is in a file or replaces an existing line using a back-referenced regular expression.
-   It's primarily useful when you want to change just a single line in a file.

### Example 1:
```yaml
- name: Ensure SELinux is set to enforcing mode 
  lineinfile: 
    path: /etc/selinux/config 
    regexp: '^SELINUX=' 
    line: SELINUX=enforcing
```
This sets the value of **SELINUX=enforcing**.

### Example 2:
```yaml
- name: Add a line to a file if the file does not exist, without passing regexp 
  lineinfile: 
    path: /etc/resolv.conf 
    line: 192.168.69.10 foo.lab.net foo
    create: yes
```
This adds an entry for the IP and hostname in the **resolv.conf** file.



## Module 10: Command

One of the most basic but useful modules, the [command module](https://docs.ansible.com/ansible/latest/modules/command_module.html) takes the command name followed by a list of space-delimited arguments.

### Example 1:
```yaml
- name: return motd to registered var
  command: cat /etc/motd 
  register: mymotd
```
### Example 2:
```yaml
- name: Change the working directory to somedir/ and run the command as db_owner if /path/to/database does not exist. 
  command: /usr/bin/make_database.sh db_user db_name 
  become: yes 
  become_user: db_owner 
  args: 
    chdir: somedir/ 
    creates: /path/to/database
```

## Sources
- https://opensource.com/article/19/9/must-know-ansible-modules
- https://www.javatpoint.com/ansible-modules
